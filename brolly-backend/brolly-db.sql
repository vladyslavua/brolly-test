DROP DATABASE IF EXISTS brollydb;
CREATE DATABASE brollydb;

\c brollydb;

CREATE TABLE items (
  ID SERIAL PRIMARY KEY,
  name VARCHAR,
  capacity INTEGER,
  premium_price NUMERIC,
  purchases INTEGER
);

INSERT INTO items (name, capacity, premium_price, purchases)
  VALUES ('iPhone X', 64, 50.42, 0),
  ('Google Pixel 2', 64, 40.0, 0),
  ('Samsung Galaxy S9', null, 45.99, 0);