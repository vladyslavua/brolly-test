var promise = require('bluebird');

var options = {
    // Initialization Options
    promiseLib: promise
};

var pgp = require('pg-promise')(options);
var connectionString = 'postgres://localhost:5432/brollydb';
var db = pgp(connectionString);

// add query functions

module.exports = {
    getAllItems: getAllItems,
    updateItem: updateItem,
    getItem: getItem
};

function getAllItems(req, res, next) {
    db.any('select * from items')
        .then(function (data) {
            data = data.map(item => {
                item.premium_price = parseFloat(item.premium_price);
                return item;
            });
            res.status(200)
                .json({
                    status: 'success',
                    data: data,
                    message: 'Retrieved ALL items'
                });
        })
        .catch(function (err) {
            return next(err);
        });
}

function updateItem(req, res, next) {
    var itemID = parseInt(req.params.id);
    db.none('update items set purchases = purchases + 1 where id = $1', [itemID])
        .then(function () {
            res.status(200)
                .json({
                    status: 'success',
                    message: 'Updated item'
                });
        })
        .catch(function (err) {
            return next(err);
        });
}

function getItem(req, res, next) {
    var itemID = parseInt(req.params.id);
    db.one('select * from items where id = $1', itemID)
        .then(function (data) {
            res.status(200)
                .json({
                    status: 'success',
                    data: data,
                    message: 'Retrieved ONE item'
                });
        })
        .catch(function (err) {
            return next(err);
        });
}