#TECHINCAL INFO
The project was created using NodeJS and Express framework, as these technologies are considered to be the quickest in bootstrapping and have enough flexibility for growth. The project is used to support front-end which is located in `front-end` folder.

# API INFO
Simple backend with REST API, which has two endpoints:
- GET '/api/items' - to get all items;
- PUT '/api/items/:id' - to update number of purchases.

# DATABASE
To run backend you need Postgres to be installed.
To run initial schema, use command: 'psql {DB NAME} -f brolly-db.sql'.

If no root role, use following commands:
CREATE ROLE root WITH PASSWORD 'root';
ALTER ROLE root WITH LOGIN;
GRANT ALL PRIVILEGES ON TABLE items TO root;

# HOW TO RUN
To run backend install modules from NPM first by running command `npm install` and then do `npm start`.