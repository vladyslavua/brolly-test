var express = require('express');
var router = express.Router();

var db = require('../queries');

/* GET home page. */
router.get('/', function(req, res, next) {
  res.render('index', { title: 'Express' });
});

router.get('/api/items', db.getAllItems);
router.get('/api/items/:id', db.getItem);
router.put('/api/items/:id', db.updateItem);

module.exports = router;