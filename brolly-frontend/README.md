This project was generated with [Angular CLI](https://github.com/angular/angular-cli).

# HOW TO RUN THE PROJECT
To run project in dev mode - run `npm start`. Before that you should init project with `npm install`. Backend must be running. Please find backend in `brolly-backend` folder and follow README instructions.

# CHALLENGE INFORMATION
The task was to create an online store, where user can see the list on items and see detailed information for each, before purchasing.
In this project I tried to create basic app with routing (selecting item), services (API requests to get all data, get specific item, update item), form validation (after selecting item, for email only) etc.
This app can be further improved by adding more features, improving UI, adding lazy loading modules (if it growth), server side rendering (for OG and SEO), visual loaders (in case of poor network connection) and other stuff.

