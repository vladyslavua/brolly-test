import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ShopComponent } from './shop/shop.component';
import { PurchaseComponent } from './purchase/purchase.component';

const userRoutes: Routes = [
  { path: '',
    redirectTo: '/shop',
    pathMatch: 'full'
  },
  { path: 'shop', component: ShopComponent },
  { path: 'shop/:id', component: PurchaseComponent}
];

@NgModule({
  imports: [
    RouterModule.forRoot(userRoutes)
  ],
  exports: [
    RouterModule
  ],
})
export class AppRoutingModule { }
