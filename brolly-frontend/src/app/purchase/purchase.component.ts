import { Component, OnInit, OnDestroy } from '@angular/core';
import { ShopService } from '../shop/shop.service';
import { ActivatedRoute, Router } from '@angular/router';
import { Subject } from 'rxjs/Subject';
import { takeUntil } from 'rxjs/operators';
import { ShoppingItems } from '../models/shopping-items';
import { ToastrService } from 'ngx-toastr';
import { FormGroup, FormControl, FormBuilder, Validators } from '@angular/forms';
import { Location } from '@angular/common';

@Component({
  selector: 'app-purchase',
  templateUrl: './purchase.component.html',
  styleUrls: ['./purchase.component.scss']
})
export class PurchaseComponent implements OnInit, OnDestroy {
  private ngUnsubscribe: Subject<any> = new Subject();
  loading = false;
  loadingSubmission = false;
  itemData: ShoppingItems = null;
  purchaseForm: FormGroup;
  formErrors = {
    'email': ''
  };
  validationMessages = {
    'email': {
      'email': 'Email is not valid'
    }
  };

  constructor(private shopService: ShopService, private route: ActivatedRoute, private router: Router, private toastr: ToastrService,
              private fb: FormBuilder, private location: Location) { }

  ngOnInit() {
    this.route
      .params
      .pipe(takeUntil(this.ngUnsubscribe))
      .subscribe(params => {
        const id = params['id'];
        this.fetchItem(id);
      });
  }

  fetchItem(id) {
    this.loading = true;
    this.shopService.fetchSingleData(id)
      .pipe(takeUntil(this.ngUnsubscribe))
      .subscribe(
        (data: any) => {
          this.loading = false;
          this.itemData = data.data;
          this.buildForm();
        },
        error => {
          this.loading = false;
          console.log(error);
        });
  }

  submitItem(id) {
    console.log(this.purchaseForm);
    if (this.formErrors.email || this.purchaseForm.status === 'INVALID') {
      this.toastr.error('Please double check your email.');
      return false;
    }
    this.loadingSubmission = true;
    this.shopService.updateSingleData(id)
      .pipe(takeUntil(this.ngUnsubscribe))
      .subscribe(
        (data: any) => {
          this.loadingSubmission = false;
          this.toastr.success('Insurance has been purchased.');
          this.router.navigate(['/shop']);
        },
        error => {
          this.toastr.success('Something went wrong. Try again in a while or contact our support.');
          this.loadingSubmission = false;
          console.log(error);
        });
  }

  onBlurMethod() {
    this.onValueChanged();
  }

  buildForm(): void {
    this.purchaseForm = this.fb.group({
      'email': ['', [
        Validators.email
      ]
      ]
    });
    this.onValueChanged();

  }

  onValueChanged() {
    if (!this.purchaseForm) { return; }
    const form = this.purchaseForm;
    for (const field in this.formErrors) {
      // clear previous error message (if any)
      this.formErrors[field] = '';
      const control = form.get(field);
      if (control && control.dirty && !control.valid) {
        const messages = this.validationMessages[field];
        for (const key in control.errors) {
          this.formErrors[field] = messages[key];
          break;
        }
      }
    }
  }

  ngOnDestroy() {
    this.ngUnsubscribe.next();
    this.ngUnsubscribe.complete();
  }

  backClicked() {
    this.location.back();
  }

}
