import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable()
export class ShopService {

  constructor(private http: HttpClient) { }

  fetchData() {
    return this.http.get('/api/items');
  }

  fetchSingleData(id) {
    return this.http.get('/api/items/' + id);
  }

  updateSingleData(id) {
    return this.http.put('/api/items/' + id, null);
  }

}
