import { Component, OnInit, OnDestroy } from '@angular/core';
import { ShopService } from './shop.service';
import { Subject } from 'rxjs/Subject';
import { takeUntil } from 'rxjs/operators';
import { ShoppingItemsResp } from '../models/shopping-items-resp';
import { ShoppingItems } from '../models/shopping-items';

@Component({
  selector: 'app-shop',
  templateUrl: './shop.component.html',
  styleUrls: ['./shop.component.scss']
})
export class ShopComponent implements OnInit, OnDestroy {
  private ngUnsubscribe: Subject<any> = new Subject();
  loading = false;
  itemsData: ShoppingItems[] = [];

  constructor(private shopService: ShopService) { }

  ngOnInit() {
    this.loading = true;
    this.shopService.fetchData()
      .pipe(takeUntil(this.ngUnsubscribe))
      .subscribe(
        (data: ShoppingItemsResp) => {
          this.loading = false;
          this.itemsData = data.data;
        },
        error => {
          this.loading = false;
          console.log(error);
        });
  }

  ngOnDestroy() {
    this.ngUnsubscribe.next();
    this.ngUnsubscribe.complete();
  }

}
