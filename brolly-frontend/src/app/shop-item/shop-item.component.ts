import { Component, Input, OnInit, OnDestroy } from '@angular/core';
import { Subject } from 'rxjs/Subject';
import { takeUntil } from 'rxjs/operators';
import { ShoppingItems } from '../models/shopping-items';

@Component({
  selector: 'app-shop-item',
  templateUrl: './shop-item.component.html',
  styleUrls: ['./shop-item.component.scss']
})
export class ShopItemComponent implements OnInit, OnDestroy {
  private ngUnsubscribe: Subject<any> = new Subject();
  item: ShoppingItems = null;

  @Input()
  set data(data: ShoppingItems) {
    this.item = data;
  }

  constructor() { }

  ngOnInit() {
  }

  ngOnDestroy() {
    this.ngUnsubscribe.next();
    this.ngUnsubscribe.complete();
  }

}
