import { ShoppingItems } from './shopping-items';

export interface ShoppingItemsResp {
  status: string;
  message: string;
  data: ShoppingItems[];
}
