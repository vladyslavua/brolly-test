export interface ShoppingItems {
  id: number;
  name: string;
  capacity: number;
  premium_price: number;
  purchases: number;
}
